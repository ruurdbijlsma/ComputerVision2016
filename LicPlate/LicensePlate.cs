﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LicPlate
{
    public class LicenseCharacter
    {
        public string Character { get; set; }

        public double Error { get; set; }

        public double Confidence { get; set; }

        public new string ToString()
        {
            return Character + " " + Math.Round(Error, 2);
        }

        public LicenseCharacter(string character) : this(character, -1) { }
        public LicenseCharacter(string character, double error) : this(character, -1, -1) { }
        public LicenseCharacter(string character, double error, double confidence)
        {
            Character = character;
            Error = error;
            Confidence = confidence;
        }
    }

    public class LicensePlate
    {
        private double conf;

        public LicensePlate()
        {
            Confidence = -1;
            Characters = new List<LicenseCharacter>();
        }
        private double GetSmallestConfidence()
        {
            return Characters.Select(t => t.Confidence).Concat(new[] {double.MaxValue}).Min();
        }

        public double Confidence
        {
            get
            {
                return conf != -1 ? conf : GetSmallestConfidence();
            }
            set
            {
                conf = value;
            }
        }
        public List<LicenseCharacter> Characters { get; }

        public new string ToString()
        {
            return GetLicensePlateString() + " " + Math.Round(Confidence, 2) + " " + GetLicensePlateErrorsString();
        }

        public string GetLicensePlateErrorsString()
        {
            string res = Characters.Aggregate("", (current, c) => current + " " + Math.Round(c.Error, 2));
            return res.Trim();
        }

        public string GetLicensePlateString()
        {
            return Characters.Aggregate("", (current, c) => current + c.Character);
        }
    }
}