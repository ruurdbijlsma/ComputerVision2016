﻿using System;

namespace LicPlate
{
    public class LicensePlateMatcher
    {
        /*
         *  Description:
         *      Find the largest license plate in the image
         *      - Segment using ThresholdHSVchannels
         *      - Remove blobs which are not license plates
         *  Input:
         *      //Original image
         *      RGB888Image plateImage	
         *  Output:
         *      //Segmented license plate
         *      ref Int32Image binaryPlateImage
         *  Return:	
         *      //License plate found?
         *      bool 
         */
        public static bool FindPlate(RGB888Image plateImage, ref Int32Image binaryPlateImage)
        {
            try
            {
                //Constants
                const int hueLowThreshold = 21;
                const int hueHighThreshold = 40;
                const int satLowThreshold = 130;
                const int satHighThreshold = 255;
                const int valLowThreshold = 110;
                const int valHighThreshold = 255;
                const int removeBlobsAreaLow = 1;
                const int removeBlobsAreaHigh = 500;
                const double removeBlobsLengthBreathRatioLow = 2;
                const double removeBlobsLengthBreathRatioHigh = 6;

                //*******************************//
                //** Exercise:                 **//
                //**   adjust licenseplate     **//
                //**   segmentation            **//
                //*******************************//            

                //Find licenseplate
                HSV888Image plateImageHSV = new HSV888Image();
                //Convert to RGB to HSV
                VisionLab.FastRGBToHSV(plateImage, plateImageHSV);

                //Threshold HSV image
                VisionLab.Threshold3Channels(plateImageHSV, binaryPlateImage, hueLowThreshold, hueHighThreshold, satLowThreshold, satHighThreshold, valLowThreshold, valHighThreshold);

                //Remove blobs with small areas
                VisionLab.RemoveBlobs(binaryPlateImage, Connected.EightConnected, BlobAnalyse.BA_Area, removeBlobsAreaLow, removeBlobsAreaHigh);
                VisionLab.RemoveBorderBlobs(binaryPlateImage, Connected.EightConnected, Border.AllBorders);

                //Remove blobs that aren't shaped like the licenseplate
                Int32Image mask = new Int32Image(binaryPlateImage);
                VisionLab.RemoveBlobs(mask, Connected.EightConnected, BlobAnalyse.BA_LengthBreadthRatio,
                    removeBlobsLengthBreathRatioLow, removeBlobsLengthBreathRatioHigh, UseXOrY.UseX);
                VisionLab.Not(mask);
                VisionLab.Multiply(binaryPlateImage, mask);

                //Fill up characters
                VisionLab.FillHoles(binaryPlateImage, Connected.FourConnected);

                mask.Dispose();
                plateImageHSV.Dispose();
                //Return true, if pixels found and there's only one blob in the image
                return (VisionLab.SumIntPixels(binaryPlateImage) > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("FindPlate: " + ex.Message);
            }
        }

        /* 
         *  Description:
         *      Locates the characters of the license plate
         *      - Warp image (Rectify)
         *      - Segment characters
         *      - Remove blobs which are to small (Lines between characters)
         *  Input:
         *      //Original image
         *      RGB888Image plateImage
         *      //Segmented license plate
         *      Int32Image binaryPlateImage
         *      Output:
         *      //Image containing binary six characters	
         *      ref Int32Image binaryCharacterImage 
         *  Return:
         *      //Function executed successfully
         *      bool
         */
        public static bool FindCharacters(RGB888Image plateImage, Int32Image binaryPlateImage, ref Int32Image binaryCharacterImage)
        {
            try
            {
                //Constants
                const int height = 100;
                const int width = 470;
                const int removeBlobsAreaLow = 1;
                const int removeBlobsAreaHigh = 400;

                XYCoord leftTop = new XYCoord();
                XYCoord rightTop = new XYCoord();
                XYCoord leftBottom = new XYCoord();
                XYCoord rightBottom = new XYCoord();


                //Find licenseplate
                VisionLab.FindCornersRectangle(binaryPlateImage, Connected.EightConnected, 0.5, Orientation.Landscape, leftTop, rightTop, leftBottom, rightBottom);
                if (!VisionLab.WarpCoordsValid(new Coord2D(leftTop), new Coord2D(rightTop), new Coord2D(leftBottom), new Coord2D(rightBottom)))
                    return false;

                Int32Image plateImageGray = new Int32Image();
                VisionLab.Convert(plateImage, plateImageGray);

                //Rectify plate
                VisionLab.Warp(plateImageGray, binaryCharacterImage, TransformDirection.ForwardT, new Coord2D(leftTop), new Coord2D(rightTop), new Coord2D(leftBottom), new Coord2D(rightBottom), height, width, 0);


                plateImageGray.Dispose();

                //*******************************//
                //** Exercise:                 **//
                //**   adjust licenseplate     **//
                //**   segmentation            **//
                //*******************************//

                //Find dark text on bright licenseplate using ThresholdISOData
                VisionLab.ThresholdIsoData(binaryCharacterImage, ObjectBrightness.DarkObject);

                //Remove small blobs and noise
                Int32Image binaryCharacterImageCopy = new Int32Image(binaryCharacterImage);
                VisionLab.Opening(binaryCharacterImageCopy, binaryCharacterImage, new Mask_Int32(5, 1, 1));

                //Remove blobs connected to the border
                VisionLab.RemoveBorderBlobs(binaryCharacterImage, Connected.EightConnected, Border.AllBorders);
                //Remove small blobs
                VisionLab.RemoveBlobs(binaryCharacterImage, Connected.EightConnected, BlobAnalyse.BA_Area, removeBlobsAreaLow, removeBlobsAreaHigh);

                leftTop.Dispose();
                rightTop.Dispose();
                leftBottom.Dispose();
                rightBottom.Dispose();                

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("FindCharacters: " + ex.Message);
            }
        }

        /*
         *  Description:
         *      Read the license plate
         *  Input:
         *      //Rectified license plate image containing six characters	
         *      Int32Image labeledRectifiedPlateImage
         *  Output:
         *      //Result by the blob matcher
         *      ref LicensePlate result
         *  Return:
         *      //six characters found
         *      bool 
        */
        public static bool MatchPlate(Int32Image binaryCharacterImage, BlobMatcher_Int32 matcher, ClassLexicon lexicon, ref LicensePlate result, ref LicensePlate lexiconResult)        
        {            
            try
            {
                //Check if 6 characters/blobs have been found and label image.
                if (VisionLab.LabelBlobs(binaryCharacterImage, Connected.EightConnected) != 6)
                    return false;

                //Calculate dimensions and locations of all characters/blobs.
                vector_BlobAnalyse baVec = new vector_BlobAnalyse
                {
                    BlobAnalyse.BA_TopLeft,
                    BlobAnalyse.BA_Height,
                    BlobAnalyse.BA_Width
                };
                vector_Blob returnBlobs = new vector_Blob();
                VisionLab.BlobAnalysis(binaryCharacterImage, VisionLab.VectorToSet_BlobAnalyse(baVec), VisionLab.MaxPixel(binaryCharacterImage), returnBlobs, SortOrder.SortDown, BlobAnalyse.BA_TopLeft, UseXOrY.UseX);
                baVec.Dispose();
                Int32Image binaryCharacter = new Int32Image();

                //Create data structure for lexicon.
                vector_vector_LetterMatch match = new vector_vector_LetterMatch();

                //Process each character/blob.
                foreach (Blob b in returnBlobs)
                {
                    //Cut out character
                    VisionLab.ROI(binaryCharacterImage, binaryCharacter, b.TopLeft(), new HeightWidth(b.Height(), b.Width()));
                    //Convert ROI result to binary
                    VisionLab.ClipPixelValue(binaryCharacter, 0, 1);
                    
                    //Calculate character's classification for all classes.
                    vector_PatternMatchResult returnMatches = new vector_PatternMatchResult();
                    float conf = matcher.AllMatches(binaryCharacter, -0.5f, 0.5f, returnMatches);
                    float err = returnMatches[0].error;
                    int id = returnMatches[0].id;
                    string chr = matcher.PatternName(id);

                    //Fill datastructure for lexicon.
                    match.Add(VisionLabEx.PatternMatchResultToLetterMatch(returnMatches));

                    //Store best match in result
                    result.Characters.Add(new LicenseCharacter(chr, err, conf));
                }

                //Validate match with lexicon.
                vector_int bestWord = new vector_int();
                lexiconResult.Confidence = lexicon.FindBestWord(match, bestWord, Optimize.OptimizeForMinimum);
                foreach (int word in bestWord)
                {
                    string character = matcher.PatternName(word);
                    //Store lexicon result
                    lexiconResult.Characters.Add(new LicenseCharacter(character));
                }

                binaryCharacter.Dispose();
                returnBlobs.Dispose();
                match.Dispose();
                bestWord.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("MatchPlate: " + ex.Message);
            }
        }
    }
}

